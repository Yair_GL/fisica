package fisica;

import java.util.Scanner;
public class Prueba{
  static Scanner sc = new Scanner(System.in);
  static final String s = "------------------------------------------------------";

  public static void menuPrincipal(){
    System.out.println();
    System.out.println(s +"\n\tMRU Y MRUA");
    System.out.println("Escribe el número de la opción deseada");
    System.out.println("\n\t1. MRU");
    System.out.println("\t2. MRUA");
    System.out.println("\t0. Salir");
    System.out.print("\nIngresa la opcion que deseas: ");
  }

  public static void calcularMRU(){
    MRU test = new MRU();
    test.inicializarMRU();
  }

  public static void calcularMRUA(){
    MRUA test1 = new MRUA();
    test1.inicializarMRUA();
  }

  public static void main(String[] args){
    byte opc = 0;
    do{
          menuPrincipal();
          try{
          opc = sc.nextByte();
          }catch(Exception n){
                      System.out.println("¡ERROR!, Ingresa solo números");
                      main(new String[1]);
                              }
        switch(opc){
            case 1:
                    calcularMRU();
                    break;
            case 2:
                    calcularMRUA();
                    break;
            case 0:
                    System.out.println("Saliendo...");
                    break;
            default:
                    System.out.println("\n\t¡Ingresa un opcion valida!");
                    break;
          }

        }while(opc != 0);


      }
}//fin clase
