package fisica;

import java.util.Scanner;
public class MRU{
  static Scanner sc = new Scanner(System.in);
  private double distancia;
  private double velocidad;
  private double tiempo;
  private final String uk = "desconocido";
  private final String s = "------------------------------------------------------";

  public MRU(){

  }

  public void inicializarMRU(){
        String distanciaS, velocidadS, tiempoS;

        System.out.println("\n" + s + "\nMovimiento Rectilíneo Uniforme");

        //figura
        System.out.println();
        System.out.println("   / \\");
        System.out.println("  / d \\");
        System.out.println(" /-----\\");
        System.out.println("/ v | t \\");
        System.out.println("---------");
        System.out.println();

        System.out.println("Ingresa los datos con los que cuentas (VERIFICA ), si no tienes alguno solo presiona la tecla ENTER");
        System.out.print("Ingresa la distancia: ");
        distanciaS = sc.nextLine();
        System.out.print("Ingresa la velocidad: ");
        velocidadS = sc.nextLine();
        System.out.print("Ingresa el tiempo: ");
        tiempoS = sc.nextLine();

        if(distanciaS.isBlank() && tiempoS.isBlank() && velocidadS.isBlank()){
            System.out.println("No ingresaste ningun dato por lo que es imposible realizar los calculos, intenta de nuevo con más de 1 dato.");
        }

        else if(distanciaS.isBlank() && tiempoS.isBlank()){
            velocidad = Double.parseDouble(velocidadS);
            System.out.println("Los datos que ingresaste son lo siguientes: ");
            System.out.println("Distancia = " + uk);
            System.out.println("Velocidad = " + velocidad);
            System.out.println("Tiempo = " + uk);
            System.out.println("Solo contamos con un solo dato por lo que es imposible calcular los demas, intenta de nuevo con más de 1 dato.");
        }

        else if(velocidadS.isBlank() && tiempoS.isBlank()){
            distancia = Double.parseDouble(distanciaS);
            System.out.println("Los datos que ingresaste son lo siguientes: ");
            System.out.println("Distancia = " + distancia);
            System.out.println("Velocidad = " + uk);
            System.out.println("Tiempo = " + uk);
            System.out.println("Solo contamos con un solo dato por lo que es imposible calcular los demas, intenta de nuevo con más de 1 dato.");
        }

        else if(distanciaS.isBlank()){
            velocidad = Double.parseDouble(velocidadS);
            tiempo = Double.parseDouble(tiempoS);
            System.out.println("Los datos que ingresaste son lo siguientes: ");
            System.out.println("Distancia = " + uk);
            System.out.println("Velocidad = " + velocidad);
            System.out.println("Tiempo = " + tiempo);
            System.out.println("Ahora procederemos a calcular la distancia...");
            System.out.println("\t\nLa distancia es: " + String.format("%.2f",calcularDistancia(velocidad,tiempo)));
        }

        else if(velocidadS.isBlank()){
            distancia = Double.parseDouble(distanciaS);
            tiempo = Double.parseDouble(tiempoS);
            System.out.println("Los datos que ingresaste son lo siguientes: ");
            System.out.println("Distancia = " + distancia);
            System.out.println("Velocidad = " + uk );
            System.out.println("Tiempo = " + tiempo);
            System.out.println("Ahora procederemos a calcular la velocidad...");
            System.out.println("\t\nLa velocidad es: " + String.format("%.2f",calcularVelocidad(distancia,tiempo)));
        }

        else if(tiempoS.isBlank()){
            distancia = Double.parseDouble(distanciaS);
            velocidad = Double.parseDouble(velocidadS);
            System.out.println("Los datos que ingresaste son lo siguientes: ");
            System.out.println("Distancia = " + distancia);
            System.out.println("Velocidad = " + velocidad );
            System.out.println("Tiempo = " + uk);

            System.out.println();
            System.out.println("   /    \\");
            System.out.println("  / " + distancia + " \\" );
            System.out.println(" /--------\\");
            System.out.println("/ " + velocidad + "|" + " t   \\");
            System.out.println("------------");
            System.out.println();

            System.out.println("Ahora procederemos a calcular el tiempo...");
            System.out.println("\t\nLa velocidad es: " + String.format("%.2f",calcularTiempo(distancia,velocidad)));
        }

        else if(distanciaS.isBlank() && velocidadS.isBlank()){
            tiempo = Double.parseDouble(tiempoS);
            System.out.println("Los datos que ingresaste son lo siguientes: ");
            System.out.println("Distancia = " + uk);
            System.out.println("Velocidad = " + uk);
            System.out.println("Tiempo = " + tiempo);
            System.out.println("Solo contamos con un solo dato por lo que es imposible calcular los demas, intenta de nuevo con más de 1 dato.");
        }

        else{

            distancia = Double.parseDouble(distanciaS);
            tiempo = Double.parseDouble(tiempoS);
            velocidad = Double.parseDouble(velocidadS);
            boolean d=false,v=false,t=false;
            System.out.println("\nLos datos que ingresaste son lo siguientes: ");
            System.out.println("\tDistancia = " + distancia);
            System.out.println("\tVelocidad = " + velocidad );
            System.out.println("\tTiempo = " + tiempo);

                if(Double.compare(distancia, calcularDistancia(velocidad,tiempo))==0 ){
                    d=true;
                }
                else if(Double.compare(velocidad,calcularVelocidad(distancia,tiempo))==0){
                    v=true;
                }
                else if(Double.compare(tiempo,calcularTiempo(distancia,velocidad))==0){
                    t=true;
                }

            if(d && v && t){
            System.out.println("Cuentas con todos los datos, no queda nada por calcular, verificalos...");
            }

            else if(!d && v && t){
            System.out.println("Cuentas con todos los datos, pero tu distancia es incorrecta..." + "\n Distancia correcta = " + calcularDistancia(velocidad,tiempo));
            }

            else if(d && !v && t){
            System.out.println("Cuentas con todos los datos, pero tu velocidad es incorrecta..." + "\n Velocidad correcta = " + calcularVelocidad(distancia,tiempo));
            }
            else if(d && v && !t){
            System.out.println("Cuentas con todos los datos, pero tu tiempo es incorrecto..." + "\n Tiempo correcta = " + calcularTiempo(distancia,velocidad));
            }
            else{
            System.out.println("Ingresaste todos los datos, pero puede ser que sean incorrectos, deja alguna variable en blanco para verificarlos...");
            }


        }




  }

  public double calcularDistancia(double velocidad, double tiempo){
      return velocidad*tiempo;
  }

  public double calcularVelocidad(double distancia, double tiempo){
      return distancia/tiempo;
  }

  public double calcularTiempo(double distancia, double velocidad){
      return distancia/velocidad;
  }




}
