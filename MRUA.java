package fisica;

import java.util.Scanner;

public class MRUA{
  static Scanner sc = new Scanner(System.in);
  private final String uk = "desconocido";
  private final String s = "------------------------------------------------------";
  private double vInicial;
  private double vFinal;
  private double aceleracion;
  private double tiempo;
  private double distancia;
  private double pInicial;
  private double pFinal;
  private double vInicialX;
  private double vInicialY;
  private double angulo;
  private double xMax;
  private double yMax;
  
  public MRUA(){
      
  }
  
  public void inicializarMRUA(){
      String vInicialS, vFinalS, aceleracionS, tiempoS, distanciaS, pFinalS, pInicialS, vInicialYS, anguloS, xMaxS, yMaxS;
        System.out.println("\n" + s + "\nMovimiento Rectilíneo Uniformemente Acelerado");
        System.out.println("Ingresa los datos con los que cuentas (VERIFICA ), si no tienes alguno solo presiona la tecla ENTER");
        System.out.print("Ingresa la velocidad inicial(x): ");
        vInicialS = sc.nextLine();
        System.out.print("Ingresa la velocidad final: ");
        vFinalS = sc.nextLine();
        System.out.print("Ingresa la aceleracion/gravedad: ");
        aceleracionS = sc.nextLine();
        System.out.print("Ingresa el tiempo: ");
        tiempoS = sc.nextLine();
        System.out.print("Ingresa la distancia: ");
        distanciaS = sc.nextLine();
        System.out.print("Ingresa la posicion inicial o cualquiera(para calcular el tiempo): ");
        pInicialS = sc.nextLine();
        System.out.print("Ingresa la posicion final: ");
        pFinalS= sc.nextLine();
        System.out.print("Ingresa la velocidad inicial en Y: ");
        vInicialYS = sc.nextLine();
        System.out.print("Ingresa el angulo de incilinacion: ");
        anguloS = sc.nextLine();
        System.out.print("Ingresa la distancia/alcance maxima en X: ");
        xMaxS = sc.nextLine();
        System.out.print("Ingresa la distancia/alcance maxima en Y: ");
        yMaxS = sc.nextLine();
        
        if(vInicialS.isBlank() && vFinalS.isBlank() && aceleracionS.isBlank() && tiempoS.isBlank() && distanciaS.isBlank() && pInicialS.isBlank() && pFinalS.isBlank() && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            System.out.println("No ingresaste ningun dato por lo que es imposible realizar los calculas, intenta ingresando más datos.");
        }
        
        //ACELERACION vInicial, vFinal,  
        else if(!(vFinalS.isBlank()) && !(vInicialS.isBlank()) && !(tiempoS.isBlank()) && aceleracionS.isBlank() && distanciaS.isBlank() && pInicialS.isBlank() && pFinalS.isBlank() && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            vInicial = Double.parseDouble(vInicialS);
            vFinal = Double.parseDouble(vFinalS);
            tiempo = Double.parseDouble(tiempoS);

            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velocidad Inicial = " + vInicial);
            System.out.println("Velocidad Final = " + vFinal);
            System.out.println("Tiempo = " + tiempo);
            System.out.println("En base a los datos que proporcionaste calcularemos la aceleracion: ");
            System.out.println("\t\nLa aceleracion es: " + String.format("%.2f",calcularAceleracion(vInicial,vFinal,tiempo)));
        }
        
      /* if(!(vInicialS.isBlank()) && !(aceleracionS.isBlank()) && !(tiempoS.isBlank()) && vFinalS.isBlank() && distanciaS.isBlank()){
            vInicial = Double.parseDouble(vInicialS);
            aceleracion = Double.parseDouble(aceleracionS);
            tiempo = Double.parseDouble(tiempoS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velocidad Inicial = " + vInicial);
            System.out.println("Aceleracion = " + aceleracion);
            System.out.println("Tiempo = " + tiempo);            
            System.out.println("En base a los datos que proporcionaste calcularemos la Velocidad Final: ");
        } */
        
        //V FINAL Y DISTANCIA VI,A,T
        else if(!(vInicialS.isBlank()) && !(aceleracionS.isBlank()) && !(tiempoS.isBlank()) && vFinalS.isBlank() && distanciaS.isBlank() && pInicialS.isBlank() && pFinalS.isBlank()   && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            Scanner read = new Scanner(System.in);
            vInicial = Double.parseDouble(vInicialS);
            aceleracion = Double.parseDouble(aceleracionS);
            tiempo = Double.parseDouble(tiempoS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velocidad Inicial = " + vInicial);
            System.out.println("Aceleracion = " + aceleracion);
            System.out.println("Tiempo = " + tiempo);
            System.out.println("\nEn base a los datos que proporcionaste podemos realizar 2 operaciones: ");
            System.out.println("\t1. Calcular la Velocidad Final");
            System.out.println("\t2. Calcular la Distancia");
            System.out.println("\t0. Regresar a MRUA");

            System.out.print("Elige:");
            byte op = read.nextByte();
            if(op==1){
            System.out.println("\t\nLa velocidad final es: " + String.format("%.2f",calcularVelocidadFinal(vInicial,aceleracion,tiempo)));
            }
            else if(op==2){
            System.out.println("\t\nLa distancia es: " + String.format("%.2f",calcularDistancia(vInicial,aceleracion,tiempo)));
            }
            else if(op==0){
                inicializarMRUA();
            }else{
                inicializarMRUA();
            }
        }
        
        //V FINAL vInicial, aceleracion, distancia
        else if(!(vInicialS.isBlank()) && !(aceleracionS.isBlank()) && !(distanciaS.isBlank()) && tiempoS.isBlank() && vFinalS.isBlank() && pInicialS.isBlank() && pFinalS.isBlank()   && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            vInicial = Double.parseDouble(vInicialS);
            aceleracion = Double.parseDouble(aceleracionS);
            distancia = Double.parseDouble(distanciaS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velocidad Inicial = " + vInicial);
            System.out.println("Aceleracion = " + aceleracion);
            System.out.println("Distancia = " + distancia);
            System.out.println("En base a los datos que proporcionaste calcularemos la Velocidad Final: ");
            System.out.println("\t\nLa velocidad final es: " + String.format("%.2f",calcularVelocidadFinal2(vInicial,aceleracion,distancia)));
            
        }
        
        //vFinal2 vInicia, aceleracion,pFinal, pInicial
        else if(!(vInicialS.isBlank()) && !(aceleracionS.isBlank()) && !(pFinalS.isBlank()) && !(pInicialS.isBlank()) && distanciaS.isBlank() && vFinalS.isBlank() && tiempoS.isBlank() && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            vInicial = Double.parseDouble(vInicialS);
            aceleracion = Double.parseDouble(aceleracionS);
            pFinal = Double.parseDouble(pFinalS);
            pInicial = Double.parseDouble(pInicialS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velocidad Inicial = " + vInicial);
            System.out.println("Aceleracion = " + aceleracion);
            System.out.println("Posicion Final = " + pFinal);
            System.out.println("Posicion Inicial = " + pInicial);
            System.out.println("En base a los datos que proporcionaste calcularemos la Velocidad Final: ");
            distancia = pFinal-pInicial;
            System.out.println("\t\nLa velocidad final es: " + String.format("%.2f",calcularVelocidadFinal2(vInicial,aceleracion,distancia)));
        } 
        
        //TIEMPO vfinal, incial,aceleracion
        else if(!(vFinalS.isBlank()) && !(vInicialS.isBlank()) && !(aceleracionS.isBlank()) && tiempoS.isBlank() && distanciaS.isBlank() && pInicialS.isBlank() && pFinalS.isBlank() && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            Double vFinal = Double.parseDouble(vFinalS);
            Double vInicial = Double.parseDouble(vInicialS);
            Double aceleracion = Double.parseDouble(aceleracionS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velocidad Final = " + vFinal);
            System.out.println("Velcidad Inicial = " + vInicial);
            System.out.println("Aceleracion = " + aceleracion);
            System.out.println("En base a los datos que proporcionaste calcularemos el Tiempo: ");
            System.out.println("\t\nLa tiempo es: " + String.format("%.2f",calcularTiempo(vFinal,vInicial,aceleracion)));
        }
        
        //posicion pinicial, vinicial,aceleracion,tiempo
        else if(!(pInicialS.isBlank()) && !(vInicialS.isBlank()) && !(tiempoS.isBlank()) && !(aceleracionS.isBlank()) && pFinalS.isBlank() && vFinalS.isBlank() && distanciaS.isBlank() && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            Double pInicial = Double.parseDouble(pInicialS);
            Double vInicial = Double.parseDouble(vInicialS);
            Double tiempo = Double.parseDouble(tiempoS);
            Double aceleracion = Double.parseDouble(aceleracionS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velcidad Inicial = " + vInicial);
            System.out.println("Posicion Inicial = " + pInicial);
            System.out.println("Tiempo = " + tiempo);
            System.out.println("Aceleracion = " + aceleracion);
            System.out.println("En base a los datos que proporcionaste calcularemos la posicion: ");
            System.out.println("\t\nLa posicion es: " + String.format("%.2f",calcularPosicion(pInicial,vInicial,aceleracion,tiempo)));
        }
         
          //aceleracion  pFinal, pInicial, Vinicial, tiempo
        else if(!(pFinalS.isBlank()) && !(pInicialS.isBlank()) && !(vInicialS.isBlank()) && !(tiempoS.isBlank()) && aceleracionS.isBlank() && vFinalS.isBlank() && distanciaS.isBlank() && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            Double pFinal = Double.parseDouble(pFinalS);
            Double pInicial = Double.parseDouble(pInicialS);
            Double vInicial = Double.parseDouble(vInicialS);
            Double tiempo = Double.parseDouble(tiempoS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Poscion Final = " + pFinal);
            System.out.println("Posicion Inicial = " + pInicial);
            System.out.println("Velocida Inicial = " + vInicial);
            System.out.println("Tiempo = " + tiempo);
            System.out.println("En base a los datos que proporcionaste calcularemos la aceleracion: ");
            System.out.println("\t\nLa aceleracion es: " + String.format("%.2f",calcularAceleracion1(pFinal, pInicial, vInicial, tiempo)));

        }
    
        //aceleracion vFInal, vInicial, pFinal, pInicial
        else if(!(vFinalS.isBlank()) && !(vInicialS.isBlank()) && !(pFinalS.isBlank()) && !(pInicialS.isBlank()) && aceleracionS.isBlank() && tiempoS.isBlank() && distanciaS.isBlank() && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            Double vFinal = Double.parseDouble(vFinalS);
            Double vInicial = Double.parseDouble(vInicialS);
            Double pFinal = Double.parseDouble(pFinalS);
            Double pInicial = Double.parseDouble(pInicialS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Poscion Final = " + pFinal);
            System.out.println("Posicion Inicial = " + pInicial);
            System.out.println("Velocidad Inicial = " + vInicial);
            System.out.println("Velocidad Final = " + vFinal);
            System.out.println("En base a los datos que proporcionaste calcularemos la aceleracion: ");
            System.out.println("\t\nLa aceleracion es: " + String.format("%.2f",calcularAceleracion2(vFinal, vInicial, pFinal, pInicial)));
        }
        
        //vInicial pFinal, pInicial, aceleracion, tiempo
        else if(!(pFinalS.isBlank()) && !(pInicialS.isBlank()) && !(aceleracionS.isBlank()) && !(tiempoS.isBlank()) && distanciaS.isBlank() && vFinalS.isBlank() && vInicialS.isBlank()  && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            Double pFinal = Double.parseDouble(pFinalS);
            Double pInicial = Double.parseDouble(pInicialS);
            Double aceleracion = Double.parseDouble(aceleracionS);
            Double tiempo = Double.parseDouble(tiempoS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Poscion Final = " + pFinal);
            System.out.println("Posicion Inicial = " + pInicial);
            System.out.println("Aceleracion = " + aceleracion);
            System.out.println("Tiempo = " + tiempo);
            System.out.println("En base a los datos que proporcionaste calcularemos la Velocidad Inicial: ");
            System.out.println("\t\nLa velocidad inicial es: " + String.format("%.2f",calcularVelocidadInicial1(pFinal, pInicial, aceleracion, tiempo)));
            
        }
        
        //VINICIAL pFinal,aceleracion,tiempoS
        else if(!(vFinalS.isBlank()) && !(aceleracionS.isBlank()) && !(tiempoS.isBlank()) && vInicialS.isBlank() && distanciaS.isBlank() && pInicialS.isBlank() && pFinalS.isBlank() && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            Double vFinal = Double.parseDouble(vFinalS);
            Double aceleracion = Double.parseDouble(aceleracionS);
            Double tiempo = Double.parseDouble(tiempoS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velocidad Final = " + pFinal);
            System.out.println("Aceleracion = " + aceleracion);
            System.out.println("Tiempo= " + tiempo);
            System.out.println("En base a los datos que proporcionaste calcularemos la Velocidad Inicial: ");
            System.out.println("\t\nLa velocidad inicial es: " + String.format("%.2f",calcularVelocidadInicial2(vFinal,aceleracion,tiempo)));
        }
        
        //vINICIAL vFinal,aceleracion,pFinal,pInicial
        else if(!(vFinalS.isBlank()) && !(aceleracionS.isBlank()) && !(pFinalS.isBlank()) && !(pInicialS.isBlank()) && vInicialS.isBlank() && tiempoS.isBlank() && distanciaS.isBlank() && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            Double vFinal = Double.parseDouble(vFinalS);
            Double aceleracion = Double.parseDouble(aceleracionS);
            Double pFinal = Double.parseDouble(pFinalS);
            Double pInicial = Double.parseDouble(pInicialS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velocidad Final = " + vFinal);
            System.out.println("Aceleracion = " + aceleracion);
            System.out.println("Posicion Final = " + pFinal);
            System.out.println("Posicion Inicial = " + pInicial);
            System.out.println("En base a los datos que proporcionaste calcularemos la Velocidad Inicial: ");
            System.out.println("\t\nLa velocidad inicial es: " + String.format("%.2f",calcularVelocidadInicial3(vFinal, aceleracion, pFinal, pInicial)));
            
        }
        
                                        /* datos vInicial, vFinal, aceleracion, tiempo, distancia, pInicial, pFinal */

        //pFINAL vFinal,vInicial,pInicial,aceleracion
        else if(!(vFinalS.isBlank()) && !(vInicialS.isBlank()) && !(pInicialS.isBlank()) && !(aceleracionS.isBlank()) && tiempoS.isBlank() && distanciaS.isBlank() && pFinalS.isBlank()  && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            Double vFinal = Double.parseDouble(vFinalS);
            Double vInicial = Double.parseDouble(vInicialS);
            Double pInicial = Double.parseDouble(pInicialS);
            Double aceleracion = Double.parseDouble(aceleracionS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velocidad Final = " + vFinal);
            System.out.println("Velocidad Inicial = " + vInicial);
            System.out.println("Posicion Inicial = " + pInicial);
            System.out.println("Aceleracion = " + aceleracion);
            System.out.println("En base a los datos que proporcionaste calcularemos la Posicion Final: ");
            System.out.println("\t\nLa posicion final es: " + String.format("%.2f",calcularPosicionFinal(vFinal, vInicial, pInicial, aceleracion)));
        }
        
                                                /* datos vInicial, vFinal, aceleracion, tiempo, distancia, pInicial, pFinal */

        //pInicial pFinal,vInicial,tiempo,aceleracion
        else if(!(pFinalS.isBlank()) && !(vInicialS.isBlank()) && !(tiempoS.isBlank()) && !(aceleracionS.isBlank()) && pInicialS.isBlank() && distanciaS.isBlank() && vFinalS.isBlank() && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            Double pFinal = Double.parseDouble(pFinalS);
            Double vInicial = Double.parseDouble(vInicialS);
            Double tiempo = Double.parseDouble(tiempoS);
            Double aceleracion = Double.parseDouble(aceleracionS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velocidad Final = " + vFinal);
            System.out.println("Velocidad Inicial = " + vInicial);
            System.out.println("Tiempo = " + tiempo);
            System.out.println("Aceleracion = " + aceleracion);
            System.out.println("En base a los datos que proporcionaste calcularemos la Posicion Inicial: ");
            System.out.println("\t\nLa posicion inicial es: " + String.format("%.2f",calcularPosicionInicial1(pFinal, vInicial, tiempo, aceleracion)));
        }
        
        //pINICIAL pFinal, vInicial, vFinal, aceleracion
        else if(!(vFinalS.isBlank()) && !(vInicialS.isBlank()) && !(pFinalS.isBlank()) && !(aceleracionS.isBlank()) && tiempoS.isBlank() && pInicialS.isBlank() && distanciaS.isBlank()  && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
            Double pFinal = Double.parseDouble(pFinalS);
            Double vInicial = Double.parseDouble(vInicialS);
            Double vFinal = Double.parseDouble(vFinalS);
            Double aceleracion = Double.parseDouble(aceleracionS);
            
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velocidad Final = " + vFinal);
            System.out.println("Velocidad Inicial = " + vInicial);
            System.out.println("Posicion Final = " + pFinal);
            System.out.println("Aceleracion = " + aceleracion);
            System.out.println("En base a los datos que proporcionaste calcularemos la Posicion Inicial: ");
            System.out.println("\t\nLa posicion inicial es: " + String.format("%.2f",calcularPosicionInicial(vFinal, vInicial, pFinal, aceleracion)));
    
        }
        
        //tiro parabolico
       // vInicialS, vFinalS, aceleracionS, tiempoS, distanciaS, pFinalS, pInicialS, vInicialS, vInicialYS, anguloS, xMaxS, yMaxS;

        else if(!(vInicialS.isBlank()) && !(anguloS.isBlank()) && !(aceleracionS.isBlank()) && vFinalS.isBlank() && tiempoS.isBlank() && pInicialS.isBlank() && distanciaS.isBlank()  && vInicialYS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank() ){
            Double vInicialX = Double.parseDouble(vInicialS);
            Double angulo = Double.parseDouble(anguloS);
            Double aceleracion = Double.parseDouble(aceleracionS);
            
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velocidad Inicial en X = " + vInicialX);
            System.out.println("Angulo = " + angulo);
            System.out.println("Aceleracion/gravedad = " + aceleracion);
            System.out.println("En base a los datos que proporcionaste calcularemos el Tiempo de Vuelo, el Alcance Horizontal Maximo y la Altura Maxima");
            System.out.println("\t\nEl tiempo de vuelo es: " + String.format("%.2f",calcularTiempoVuelo(vInicialX,angulo,aceleracion)));
            System.out.println("\t\nEl alcance horizontal maximo es: " + String.format("%.2f",calcularAlcanceMaximoX(vInicialX, angulo, aceleracion)));
            System.out.println("\t\nLa altura maximo alcanzada es: " + String.format("%.2f",calcularAlturaMaximaY(vInicialX,angulo, aceleracion)));
        }
        
       else if(!(vInicialYS.isBlank()) && !(aceleracionS.isBlank()) && !(tiempoS.isBlank()) && vFinalS.isBlank() && distanciaS.isBlank() && pFinalS.isBlank() && pInicialS.isBlank()  && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank() && vInicialS.isBlank()){
           Double vInicialY = Double.parseDouble(vInicialS);
           Double aceleracion = Double.parseDouble(aceleracionS);
           Double tiempo = Double.parseDouble(tiempoS);
           
            System.out.println("Los datos que ingresaste son los siguientes: ");
            System.out.println("Velocidad Inicial en Y = " + vInicialY);
            System.out.println("Aceleracion = " + aceleracion);
            System.out.println("Tiempo = " + tiempo);
            System.out.println("En base a los datos que proporcionaste calcularemos la velocidad en Y");
            System.out.println("\t\nEl velocidad en Y es: " + String.format("%.2f",calcularVelocidadenY(vInicialY, aceleracion,tiempo)));
            
       }
        
       else if(!(vInicialS.isBlank()) && !(anguloS.isBlank()) && !(tiempoS.isBlank()) && vFinalS.isBlank() && aceleracionS.isBlank() && distanciaS.isBlank() && pFinalS.isBlank() && pInicialS.isBlank() && vInicialYS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
           Double vInicial = Double.parseDouble(vInicialS);
           Double angulo = Double.parseDouble(anguloS);
           Double tiempo = Double.parseDouble(tiempoS);
           
           System.out.println("Los datos que ingresaste son los siguientes: ");
           System.out.println("Velocidad Inicial en X = " + vInicial);
           System.out.println("Angulo = " + angulo);
           System.out.println("Tiempo = " + tiempo);
           System.out.println("En base a los datos que proporcionaste calcularemos la poiscion en X y la posicion en Y");
           System.out.println("\t\nLa posicion en X es: " + String.format("%.2f",calcularPosicionX(vInicial,angulo,tiempo)));
           System.out.println("\t\nLa posicion en Y es: " + String.format("%.2f",calcularPosicionY(vInicial,angulo,tiempo)));
           
       }
       
       else if(!(vInicialS.isBlank()) && !(anguloS.isBlank()) && vFinalS.isBlank() && aceleracionS.isBlank() && pFinalS.isBlank() && pInicialS.isBlank() && vInicialYS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
           Double vInicial = Double.parseDouble(vInicialS);
           Double angulo = Double.parseDouble(anguloS);
           
           System.out.println("Los datos que ingresaste son los siguientes");
           System.out.println("Velocidad Inicial  = " + vInicial);
           System.out.println("Angulo = " + angulo);
           System.out.println("En base a los datos que proporcionaste calcularemos la velocidad en X");
           System.out.println("\t\nLa posicion en X es: " + String.format("%.2f",calcularVelocidadX(vInicial,angulo)));
       }
        
       else if(!(vInicialYS.isBlank()) && !(anguloS.isBlank()) && vFinalS.isBlank() && aceleracionS.isBlank() && pFinalS.isBlank() && pInicialS.isBlank() && vInicialS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
           Double vInicialY = Double.parseDouble(vInicialYS);
           Double angulo = Double.parseDouble(anguloS);
           
           System.out.println("Los datos que ingresaste son los siguientes");
           System.out.println("Velocidad Inicial = " + vInicialY);
           System.out.println("Angulo = " + angulo);
           System.out.println("En base a los datos que proporcionaste calcularemos la velocidad en Y");
           System.out.println("\t\nLa posicion en X es: " + String.format("%.2f",calcularVelocidadY(vInicialY,angulo)));
       }
       
       else if(!(vInicialS.isBlank()) && !(aceleracionS.isBlank()) && vFinalS.isBlank() && anguloS.isBlank() && pFinalS.isBlank() && pInicialS.isBlank() && vInicialYS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
           Double vInicial= Double.parseDouble(vInicialS);
           Double aceleracion = Double.parseDouble(aceleracionS);
           
           System.out.println("Los datos que ingresaste son los siguientes");
           System.out.println("Velocidad Inicial = " + vInicial);
           System.out.println("Aceleracion = " + aceleracion);
           System.out.println("En base a los datos que proporcionaste calcularemos el TIEMPO PARA ALCANZAR LA ALTURA MAXIMA Y EL TIEMPO DE VUELO");
           System.out.println("\t\nEl tiempo en el que alcanza la altura maxima es: " + String.format("%.2f",calcularTiempoAlturaMaxima(vInicial,aceleracion)));
           System.out.println("\t\nEl tiempo de vuelo es: " + String.format("%.2f",calcularTiempoVuelo2(vInicial,aceleracion)));
       }
       
       else if(!(vInicialS.isBlank()) && !(anguloS.isBlank()) && !(tiempoS.isBlank()) && vFinalS.isBlank() && aceleracionS.isBlank() && pFinalS.isBlank() && pInicialS.isBlank() && vInicialYS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
           Double vInicial = Double.parseDouble(vInicialS);
           Double angulo = Double.parseDouble(anguloS);
           Double tiempo = Double.parseDouble(tiempoS);
           
           System.out.println("Los datos que ingresaste son los siguientes");
           System.out.println("Velocidad Inicial = " + vInicial);
           System.out.println("Angulo = " + angulo);
           System.out.println("Tiempo = " + tiempo);
           System.out.println("En base a los datos que proporcionaste calcularemos la velocidad en Y");
           System.out.println("\t\nLa velocidad en Y es: " + String.format("%.2f",calcularVelocidadY2(vInicial, angulo, tiempo)));
       }
       
       else if(!(pInicialS.isBlank()) && !(aceleracionS.isBlank()) && !(vInicialS.isBlank()) && vFinalS.isBlank() && tiempoS.isBlank() && distanciaS.isBlank() && pFinalS.isBlank() && vInicialYS.isBlank() && anguloS.isBlank() && xMaxS.isBlank() && yMaxS.isBlank()){
           Double posicion = Double.parseDouble(pInicialS);
           Double aceleracion = Double.parseDouble(aceleracionS);
           Double vInicial = Double.parseDouble(vInicialS);
           
           
           System.out.println("Los datos que ingresaste son los siguientes");
           System.out.println("Velocidad Inicial = " + vInicial);
           System.out.println("Aceleracion = " + aceleracion);
           System.out.println("Posicion= " + posicion);
           System.out.println("En base a los datos que proporcionaste calcularemos el tiempo");
           System.out.println("\t\nEL tiempo es: " + String.format("%.2f",calcularTiempoCuad(aceleracion,posicion,vInicial)));
           
       }
        
       else{
           System.out.println("ERRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRROR");
        }
       
       
       
       
        
    }
        
    public double calcularAceleracion(double vInicial, double vFinal, double tiempo){
        return (vFinal-vInicial)/tiempo;
    }

    public double calcularDistancia(double vInicial, double aceleracion, double tiempo){
        return vInicial*tiempo+(aceleracion/2)*Math.pow(tiempo, 2);
    }
   
    public double calcularVelocidadFinal(double vInicial, double aceleracion, double tiempo){
        return vInicial+(aceleracion*tiempo);
    }
    
    public double calcularVelocidadFinal2(double vInicial, double aceleracion, double distancia){
        return Math.sqrt(Math.pow(vInicial,2)+(2*aceleracion)*distancia);
    }
    
    public double calcularTiempo(double vFinal, double vInicial, double aceleracion){
        return (vFinal-vInicial)/aceleracion;
    }
    
    public double calcularPosicion(double pInicial, double vInicial, double aceleracion, double tiempo){
        return pInicial+(vInicial*tiempo)+(aceleracion/2)*Math.pow(tiempo,2);
    }
    
    public double calcularAceleracion1(double pFinal, double pInicial, double vInicial, double tiempo){
        return (pFinal-pInicial-(vInicial*tiempo))/((Math.pow(tiempo, 2))/2);
    }
    
    public double calcularAceleracion2(double vFinal, double vInicial, double pFinal, double pInicial){
        return (Math.pow(vFinal, 2)-Math.pow(vInicial, 2)/(2*(pFinal-pInicial)));
    }
    
    public double calcularVelocidadInicial1(double pFinal, double pInicial, double aceleracion, double tiempo){
        return (pFinal-pInicial-((aceleracion/2)*Math.pow(tiempo, 2))/tiempo);
    }
    
    public double calcularVelocidadInicial2(double vFinal, double aceleracion, double tiempo){
        return vFinal-(aceleracion*tiempo);
    }
    
    public double calcularVelocidadInicial3(double vFinal, double aceleracion, double pFinal, double pInicial){
        return Math.sqrt((Math.pow(vFinal, 2)-(2*aceleracion*(pFinal-pInicial))));
    }
    
    public double calcularPosicionFinal(double vFinal, double vInicial, double pInicial, double aceleracion){
        return (((Math.pow(vFinal, 2)-Math.pow(vInicial, 2))+pInicial)/2*aceleracion);
    }
    
    public double calcularPosicionInicial1(double pFinal, double vInicial, double tiempo, double aceleracion){
        return pFinal-(vInicial*tiempo)-(aceleracion/2)*Math.pow(tiempo, 2);
    }
    
    public double calcularPosicionInicial(double vFinal, double vInicial, double pFinal, double aceleracion){
        return -1*(((Math.pow(vFinal,2)-Math.pow(vInicial, 2))+pFinal)/2*(aceleracion));
    }
    
    //parabolic M
    public double calcularTiempoVuelo(double vInicialX, double angulo, double aceleracion){
        return (2*vInicialX*Math.sin(angulo))/aceleracion;
    } 
    
    public double calcularTiempoVuelo2(double vInicial, double aceleracion){
        return (2*vInicial)/aceleracion;    
    }
    
    
    
    public double calcularAlcanceMaximoX(double vInicialX, double angulo, double aceleracion){
        return Math.pow(vInicialX,2)*Math.sin(2*angulo)/aceleracion;
    }
   
    public double calcularAlturaMaximaY(double vInicialX, double angulo, double aceleracion){
        return (Math.pow(vInicialX, 2)*Math.sin(angulo))/(2*aceleracion);
    }

    public double calcularVelocidadenY(double vInicialY, double aceleracion, double tiempo){
        return (vInicialY+(-1*aceleracion)*tiempo);
    }
    
    public double calcularPosicionX(double vInicialX, double angulo, double tiempo){
        return vInicialX*Math.cos(angulo)*tiempo;
    }
    
    public double calcularPosicionY(double vInicial, double angulo, double tiempo){
        return vInicial*Math.cos(angulo)*tiempo;
    }
    
    public double calcularVelocidadX(double vInicialX, double angulo){
        return vInicialX*Math.cos(angulo);
    }
    
    public double calcularVelocidadY(double vInicial, double angulo){
        return vInicial*Math.sin(angulo);
    }
    
    public double calcularTiempoAlturaMaxima(double vInicial, double aceleracion){
        return vInicial/aceleracion;
    }
    
    public double calcularVelocidadY2(double vInicial, double angulo, double tiempo){
        return vInicial*(Math.sin(angulo))-aceleracion*tiempo;
    }
    
    public double calcularTiempoCuad(double aceleracion, double posicion, double vInicial){
        double t1=0,t2 = 0,r=0;
        double ac = (aceleracion/2)*-1;
        //aceleracion   vInicial posicion
        t1 = (-vInicial+(Math.sqrt(Math.pow(vInicial,2)-4*(ac)*(posicion))))/(2*ac);
        t2 = (-vInicial-(Math.sqrt(Math.pow(vInicial,2)-4*(ac)*(posicion))))/(2*ac);
        
        if(t1>0){
            r = t1;
        }else{
            r = t2;
        }
        
        return r;
    }
    
    
   
}//fin clase

